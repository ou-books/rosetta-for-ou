# Datasets {#datasets}

De voorbeelden in dit boek maken gebruik van een aantal vrij beschikbare datasets. Deze datasets worden hier beschreven.
In hoofdstuk \@ref(data-laden) wordt uitgelegd hoe datasets geladen kunnen worden.

## `pp15`

De dataset `pp15` is afkomstig van het "partypanel" studie uit 2015.
Dit is een determinantenonderzoek opgezet om de determinanten van het gebruik van een hoge dosis MDMA (is het actieve ingrediënt in tabletten die als ecstasy worden verkocht) te meten. De meeste variabelen hebben betrekking op de subdeterminanten van de drie variabelen van de "Reasoned Action Approach": houding, gepercipieerde normen en gepercipieerde gedragscontrole. Om deze data direct te downloaden, gebruik: https://gitlab.com/r-packages/rosetta/raw/prod/data/pp15.csv?inline=false.

De dataset `pp15` bevat de volgende variabelen, met labels:

```{r pp15-dataset}

data("pp15", package="rosetta");

variableLabels <-
  c(gender = "Gender",
    age = "Age (in years)",
    hasJob = "Employment status",
    jobHours = "Hours of employment",
    currentEducation = "If in education, study type",
    prevEducation = "If finished education, study type",
    currentEducation_cat = "Categorized current education type",
    prevEducation_cat = "Categorized previous education type",
    xtcUseDoseHigh = "MDMA dose (in mg) that is considered a high dose",
    xtcUseDosePref = "Preferred MDMA dose (in mg)",
    xtcUsePillHigh = "Number of pills that is considered a high dose",
    xtcUsePillPref = "Preferred number of pills",
    weight_other = "Body weight (in kg)",
    highDose_IntentionRAA_intention = "RAA intention: planning to",
    highDose_IntentionRAA_want = "RAA intention: want to",
    highDose_IntentionRAA_expectation = "RAA intention: expect to",
    highDose_IntentionFrq_freq.0 = "Intention frequency: number of times",
    highDose_IntentionFrq_freq.1 = "Intention frequency: multiplier",
    highDose_AttGeneral_good = "Attitude (direct): bad-good",
    highDose_AttGeneral_prettig = "Attitude (direct): unpleasant-pleasant",
    highDose_AttGeneral_slim = "Attitude (direct): dumb-smart",
    highDose_AttGeneral_gezond = "Attitude (direct): unhealthy-healthy",
    highDose_AttGeneral_spannend = "Attitude (direct): boring-exciting",
    highDose_NormGeneral_in1 = "Injunctive norm (direct): people important to me",
    highDose_NormGeneral_in2 = "Injunctive norm (direct): people whose opinion I value",
    highDose_NormGeneral_dn1 = "Descriptive norm (direct): people I respect and admire",
    highDose_NormGeneral_dn2 = "Descriptive norm (direct): people like me",
    highDose_PBCgeneral_ifwanted = "Perceived behavioral control (direct): would not/would be able to",
    highDose_PBCgeneral_easy = "Perceived behavioral control (direct): easy-hard",
    highDose_PBCgeneral_control = "Perceived behavioral control (direct): no/complete control",
    highDose_PBCgeneral_externalFactors = "Perceived behavioral control (direct): no/many external factors",
    highDose_PBCgeneral_notOnlyMe = "Perceived behavioral control (direct): under others' control/under my control",
    highDose_OpenWhy = "Perceived reasons for using a high dose of MDMA (open question)",
    highDose_OpenWhyNot = "Perceived reasons for *not* using a high dose of MDMA (open question)",
    highDose_AttBeliefs_long = "Expectation (attitudinal belief): shorter/longer trip",
    highDose_AttBeliefs_intensity = "Expectation (attitudinal belief): less/more intense trip",
    highDose_AttBeliefs_intoxicated = "Expectation (attitudinal belief): less/more wasted",
    highDose_AttBeliefs_energy = "Expectation (attitudinal belief): less/more energy",
    highDose_AttBeliefs_euphoria = "Expectation (attitudinal belief): less/more euphoric",
    highDose_AttBeliefs_insight = "Expectation (attitudinal belief): less/more self-insights",
    highDose_AttBeliefs_connection = "Expectation (attitudinal belief): less/more connected to others",
    highDose_AttBeliefs_contact = "Expectation (attitudinal belief): easier/harder to make contact",
    highDose_AttBeliefs_sex = "Expectation (attitudinal belief): feel less/more like sex",
    highDose_AttBeliefs_coping = "Expectation (attitudinal belief): forget problems slower/faster",
    highDose_AttBeliefs_isolated = "Expectation (attitudinal belief): less/more socially isolated",
    highDose_AttBeliefs_boundaries = "Expectation (attitudinal belief): can probe my boundaries worse/better",
    highDose_AttBeliefs_music = "Expectation (attitudinal belief): music sounds worse/better",
    highDose_AttBeliefs_hallucinate = "Expectation (attitudinal belief): less/more hallucinations",
    highDose_AttBeliefs_timeAwareness = "Expectation (attitudinal belief): time passes slower/faster",
    highDose_AttBeliefs_memory = "Expectation (attitudinal belief): remember less/more",
    highDose_AttBeliefs_health = "Expectation (attitudinal belief): less/more healthy",
    highDose_AttBeliefs_better = "Expectation (attitudinal belief): experience worse/better",
    highDose_AttBeliefs_physicalSideEffects = "Expectation (attitudinal belief): worry less/more physical side effects",
    highDose_AttBeliefs_psychicSideEffects = "Expectation (attitudinal belief): worry less/more mental side effects",
    highDose_AttBeliefs_regret = "Expectation (attitudinal belief): less/more regret",
    highDose_AttDesirable_long = "Evaluation (attitudinal belief): prefer shorter/longer trip",
    highDose_AttDesirable_intens = "Evaluation (attitudinal belief): prefer less/more intense trip",
    highDose_AttDesirable_intoxicated = "Evaluation (attitudinal belief): prefer less/more wasted",
    highDose_AttDesirable_energy = "Evaluation (attitudinal belief): prefer less/more energy",
    highDose_AttDesirable_euphoria = "Evaluation (attitudinal belief): prefer less/more euphoria",
    highDose_AttDesirable_insight = "Evaluation (attitudinal belief): prefer less/more self-insight",
    highDose_AttDesirable_connection = "Evaluation (attitudinal belief): prefer less/more connection",
    highDose_AttDesirable_contact = "Evaluation (attitudinal belief): prefer less/more contact",
    highDose_AttDesirable_sex = "Evaluation (attitudinal belief): prefer feeling like sex less/more",
    highDose_AttDesirable_coping = "Evaluation (attitudinal belief): prefer forgetting problems slower/faster",
    highDose_AttDesirable_isolated = "Evaluation (attitudinal belief): prefer feeling less/more isolated",
    highDose_AttDesirable_boundaries = "Evaluation (attitudinal belief): prefer testing my boundaries less/more",
    highDose_AttDesirable_purity = "Evaluation (attitudinal belief): prefer impure/pure pills",
    highDose_AttDesirable_music = "Evaluation (attitudinal belief): prefer music sounding worse/better",
    highDose_AttDesirable_hallucinate = "Evaluation (attitudinal belief): prefer less/more hallucination",
    highDose_AttDesirable_timeAwareness = "Evaluation (attitudinal belief): prefer time to go slower/faster",
    highDose_AttDesirable_memory = "Evaluation (attitudinal belief): prefer remembering less/more",
    highDose_NormBeliefs_partner = "Perceived disapproval/approval from partner",
    highDose_NormBeliefs_bestFriends = "Perceived disapproval/approval from best friends",
    highDose_NormBeliefs_xtcFriends = "Perceived disapproval/approval from ecstasy-using friends",
    highDose_NormBeliefs_otherFriends = "Perceived disapproval/approval from other friends",
    highDose_NormBeliefs_partyPeople = "Perceived disapproval/approval from party patrons",
    highDose_NormBeliefs_parents = "Perceived disapproval/approval from parents",
    highDose_NormBeliefs_siblings = "Perceived disapproval/approval from siblings",
    highDose_NormMTC_partner = "Motivation to comply with partner",
    highDose_NormMTC_bestFriends = "Motivation to comply with best friends",
    highDose_NormMTC_xtcFriends = "Motivation to comply with ecstasy-using friends",
    highDose_NormMTC_otherFriends = "Motivation to comply with other friends",
    highDose_NormMTC_partyPeople = "Motivation to comply with party patrons",
    highDose_NormMTC_parents = "Motivation to comply with parents",
    highDose_NormMTC_siblings = "Motivation to comply with siblings",
    highDose_DescrNorm_partner = "Perception that partner never/always uses a high dose",
    highDose_DescrNorm_bestFriends = "Perception that best friends never/always use a high dose",
    highDose_DescrNorm_xtcFriends = "Perception that ecstasy-using friends never/always use a high dose",
    highDose_DescrNorm_otherFriends = "Perception that other friends never/always use a high dose",
    highDose_DescrNorm_partyPeople = "Perception that party patrons never/always use a high dose",
    highDose_DescrNorm_parents = "Perception that parents never/always use a high dose",
    highDose_DescrNorm_siblings = "Perception that siblings never/always use a high dose",
    highDose_ContrBeliefs_sayNo = "Control belief: declining offer of high dose hard/easy",
    highDose_ContrBeliefs_highDoseEasy = "Control belief: taking high dose hard/easy",
    highDose_ContrBeliefs_normalDoseEasy = "Control belief: preventing taking high dose hard/easy",
    highDose_ContrBeliefs_easyHighDoseWhenHigh = "Control belief: taking high dose once high hard/easy",
    highDose_ContrBeliefs_easyNormalDoWhenHigh = "Control belief: preventing high dose once high hard/easy",
    highDose_Tolerance_tolerance = "Perception of own tolerance to MDMA low/high",
    highDose_attProduct_boundaries = "Attitudinal belief product: boundary testing",
    highDose_attProduct_connection = "Attitudinal belief product: connection to others",
    highDose_attProduct_contact = "Attitudinal belief product: contact with others",
    highDose_attProduct_coping = "Attitudinal belief product: forgetting problems",
    highDose_attProduct_energy = "Attitudinal belief product: energy",
    highDose_attProduct_euphoria = "Attitudinal belief product: euphoria",
    highDose_attProduct_hallucinate = "Attitudinal belief product: hallucinations",
    highDose_attProduct_insight = "Attitudinal belief product: self-insight",
    highDose_attProduct_intoxicated = "Attitudinal belief product: wasted",
    highDose_attProduct_isolated = "Attitudinal belief product: social isolation",
    highDose_attProduct_long = "Attitudinal belief product: trip duration",
    highDose_attProduct_memory = "Attitudinal belief product: remembering afterwards",
    highDose_attProduct_music = "Attitudinal belief product: music",
    highDose_attProduct_sex = "Attitudinal belief product: feeling like sex",
    highDose_attProduct_timeAwareness = "Attitudinal belief product: time acceleration",
    highDose_normProduct_bestFriends = "Normative belief product: best friends",
    highDose_normProduct_otherFriends = "Normative belief product: other friends",
    highDose_normProduct_parents = "Normative belief product: parents",
    highDose_normProduct_partner = "Normative belief product: partner",
    highDose_normProduct_partyPeople = "Normative belief product: party patrons",
    highDose_normProduct_siblings = "Normative belief product: siblings",
    highDose_normProduct_xtcFriends = "Normative belief product: ecstasy-using friends",
    highDose_intention = "Mean of direct RAA intention items",
    highDose_attitude = "Mean of direct RAA attitude items",
    highDose_perceivedNorm = "Mean of direct RAA perceived norm items ",
    highDose_pbc = "Mean of direct RAA perceived behavioral control items",
    gender_bi = "Gender, dichotomized into a binary variable",
    hasJob_bi = "Employment status, dichotomized into a binary variable"
    );

for (i in names(variableLabels)) {
  attr(pp15[, i], "label") <- variableLabels[i];
}

pp15_varOverview <-
  data.frame(column = names(pp15),
             variable = variableLabels);

knitr::kable(pp15_varOverview,
             row.names = FALSE);

```




## `Politiek_Media`


De dataset `Politie_Media` is eigenlijk een kruistabel, en bevat de volgende variabelen, met labels:

```{r PolitiekMedia-dataset}

PolitiekMedia <- haven::read_sav("data/Politiek_Media.sav")

variableLabels <-
  c(partij = "Politieke partij (GL, PVV, VVD, CDA, D66, PvdA)",
    media =  "Mediagebruik kiezers(Twitter, Facebook, Krant, Televisie, Online)"
    );

for (i in names(variableLabels)) {
  attr(PolitiekMedia[, i], "label") <- variableLabels[i];
}

PolitiekMedia_vars <-
  data.frame(column = names(PolitiekMedia),
             variable = variableLabels);

knitr::kable(PolitiekMedia_vars,
             row.names = FALSE)

```



## `statistiekangst`

Deze data bevat een experimentele interventie: een cursus om statistiekangst te verminderen. Statistiekangst is zowel voor als na de interventie gemeten. Cursustype is gemanipuleerd en deelnemers zijn gerandomiseerd. De toegepaste cursus is ontwikkeld om een verbetering te zijn ten opzichte van de traditionele cursus. 
De dataset `statistiekangst` bevat de volgende variabelen, met labels:

```{r statistiekangst-dataset}

statistiekangst <- haven::read_sav("data/statistiekangst.sav")

variableLabels <-
  c(Respnum = "Respondent nummer",
    Gender = "Geslacht (1 = man, 2 = vrouw)",
    Age = "Leeftijd (in jaren)",
    Education = "Opleidingsniveau (1=laag,2=laag/middel, 3=middel,4=middel/hoog 5=hoog",
    Cursus = "1=Traditioneel, 2=Toegepast",
    Statken1 = "Statistiekkennis voor de cursus",
    Statken2 = "Statistiekkennis na de cursus",
    Angst1 = "Statistiekangst voor de cursus",
    Angst2 = "Statistiekangst na de cursus",
    dif_Stat = "Toename statistiekkennis",
    dif_Angst = "Toename statistiekangst"
    );

for (i in names(variableLabels)) {
  attr(statistiekangst[, i], "label") <- variableLabels[i];
}

statistiekangst_vars <-
  data.frame(column = names(statistiekangst),
             variable = variableLabels);

knitr::kable(statistiekangst_vars,
             row.names = FALSE)

```



## `schoolsucces`


De dataset `schoolsucces` bevat de volgende variabelen, met labels:

```{r schoolsucces-dataset}

schoolsucces <- haven::read_sav("data/schoolsucces.sav")

variableLabels <-
  c(geslacht = "Geslacht (0 = jongen, 1 = meisje)",
    leeftijd = "Leeftijd (in jaren) bij examen",
    uitslag = "Uitslag examen (0 = geslaagd, 1 = gezakt)",
    cijfer = "Gemiddeld cijfer behaald op eindtoets",
    IQ = "Intelligentie score",
    inkomen = "Inkomen ouders in drie categorieen"
    );

for (i in names(variableLabels)) {
  attr(schoolsucces[, i], "label") <- variableLabels[i];
}

schoolsucces_vars <-
  data.frame(column = names(schoolsucces),
             variable = variableLabels);

knitr::kable(schoolsucces_vars,
             row.names = FALSE)

```

## `sportcasus`

Bij een ICT-bedrijf met in totaal 108 medewerkers is men zich bewust dat men meer moet bewegen om gezond te blijven. Vrijwel alle medewerkers zitten namelijk de hele dag achter hun computer. Om gezondheidsklachten, en dus ook ziekteverzuim, te voorkomen heeft het bedrijf een sportzaal ter beschikking gesteld waarin de medewerkers van het bedrijf onder werktijd elke dag een half uur mogen sporten. Het bedrijf heeft echter de indruk dat de medewerkers weinig gebruikmaken van deze faciliteit en wil hen motiveren er wel gebruik van te maken. Hiervoor zetten ze een experiment op. Voorafgaand aan het experiment, wordt allereerst aan alle medewerkers gevraagd in te vullen hoeveel keer per week zij een half uur sporten in de sportzaal. Omdat de onderzoekers vermoeden dat leeftijd hier een belangrijke rol bij speelt, werd de medewerkers ook naar leeftijd gevraagd. Medewerkers zijn daarna ingedeeld in jonge medewerkers, dat wil zeggen: jonger dan 45 jaar, en oudere medewerkers, medewerkers vanaf 45 tot en met 65 jaar. Ook is gevraagd naar het aantal uren dat ze volgens contract per week werken.

Er is in samenspraak met een onderzoeker en de directie van het bedrijf besloten het personeel voorlichting te geven over de voordelen van bewegen en de effecten van gezondheid. Een groep van de werknemers krijgt elke keer als ze willen sporten een persoonlijk advies. Een andere groep van de medewerkers krijgt als ze de tijd reserveren om te gaan sporten een digitale voorlichtingsfolder over de voordelen van sporten als popup. Als laatste is er een groep die zonder enige extra voorlichting naar de sportschool gaat. Om de effecten van het type voorlichting te onderzoeken wordt eerst onderzocht hoe vaak de sportschool bezocht is. Dan wordt de persoonlijke voorlichting of de digitale folder aangeboden (afhankelijk van in welke groep iemand is ingedeeld). Direct na afloop van het experiment wordt nogmaals op dezelfde wijze onderzocht hoe vaak de sportzaal bezocht is. Na een maand wordt deze meting herhaald, hierbij wordt vastgesteld hoe vaak op dat moment de sportzaal wordt bezocht door de deelnemers aan het experiment.

De dataset `sportcasus` bevat de volgende variabelen, met labels:

```{r sportcasus-dataset, eval=T}

sportcasus <- haven::read_sav("data/sportcasus.sav")
sportcasus <- sportcasus[,c(3,2,1,4,7,5,6)]

variableLabels <-
  c( leeftijd =  "Leeftijd (1 = tot en met 45, 2 = ouder dan 45) ",
     voorlichting = "Type voorlichting (1 = controlegroep, 1 = digitale folder, 2=persoonlijk advies)",
     sportscore1 = "Aantal keren gesport eerste meting (0 tot en met 5)",
     sportscore2 = "Aantal keren gesport tweede meting (0 tot en met 5)",
     sportscore3 = "Aantal keren gesport derde meting (0 tot en met 5)",
    sportverschil = "Verschil tussen meting 1 en 2 (positieve score geeft toename aan)",
    urenaanwezig = "Aantal uren contractueel aanwezig"
    );

for (i in names(variableLabels)) {
  attr(sportcasus[, i], "label") <- variableLabels[i];
}

sportcasus_vars <-
  data.frame(column = names(sportcasus),
             variable = variableLabels);

knitr::kable(sportcasus_vars,
             row.names = FALSE)

```

## `contraProductiefGedrag`

In een bedrijf wordt onderzoek gedaan naar contraproductief gedrag (CPG) op de werkvloer (zoals: te laat komen, stelen, roddelen over collega's, lage kwaliteit van werk). Men vermoedt dat dit samenhangt met het vertrouwen in het management, met een cynische houding ten opzichte van de organisatie en van de stemming van een werknemer. Verder denkt men dat verschillende vormen van gepercipieerde rechtvaardigheid invloed kunnen hebben op dit CPG. 

De dataset `contraProductiefGedrag` bevat de volgende variabelen, met labels:

```{r contraProductiefGedrag-dataset}

contraProductiefGedrag <- haven::read_sav("data/contraProductiefGedrag.sav")

variableLabels <-
  c(nummer = "Respondentnummer",
    bedrijf = "Organisatienummer",
    sekse = "vrouw, man",
    leeftijd = "Leeftijd respondent",
    opleiding = "Opleiding respondent",
    vertrouwen = "Vertrouwen in management",
    cynisme = "Cynisme over organisatie",
    negatiefAffect = "Negatief affect van respondent",
    contraProductiefGedrag = "Contraproductief gedrag door respondent",
    procedureleRechtvaardigheid = "Ervaren procedurele rechtvaardigheid",
    distributieveRechtvaardigheid = "Ervaren distributieve rechtvaardigheid",
    interactioneleRechtvaardigheid = "Ervaren interactionele rechtvaardigheid"
    );

for (i in names(variableLabels)) {
  attr(contraProductiefGedrag[, i], "label") <- variableLabels[i];
}

contraProductiefGedrag_vars <-
  data.frame(column = names(contraProductiefGedrag),
             variable = variableLabels);

knitr::kable(contraProductiefGedrag_vars,
             row.names = FALSE)

```


## `wiskunde`

In verschillende klassen zijn data verzameld om te kijken of de score op wiskundetoetsen kunnen worden voorspeld uit de intelligentie van de leerling en uit enkele karakteristieken van de klas en de leerkracht.

De dataset `wiskunde` bevat de volgende variabelen, met labels:

```{r wiskunde-dataset}

wiskunde <- haven::read_sav("data/wiskunde.sav")

variableLabels <-
  c(klas = "Klasnummer",
    wiskunde = "Wiskundescore",
    intelligentie = "Intelligentiescore",
    ervaring = "Ervaring leerkracht",
    klasGrootte = "Aantal leerlingen in klas",
    sekseLeerling = "Sekse van de leerling",
    sekseLeerkracht = "Sekse van de leerkracht"
    );

for (i in names(variableLabels)) {
  attr(wiskunde[, i], "label") <- variableLabels[i];
}

wiskunde_vars <-
  data.frame(column = names(wiskunde),
             variable = variableLabels);

knitr::kable(wiskunde_vars,
             row.names = FALSE)

```

## `roken`

In een intensieve longitudinale studie onder mensenn die willen stoppen met roken, wordt gekeken of het roken van een sigaret samenhangt met de stress die men op een moment op de dag ervaart. 

De dataset `roken` bevat de volgende variabelen, met labels:

```{r roken-dataset}

roken <- haven::read_sav("data/roken.sav")

variableLabels <-
  c(respondent = "Respondentnummer",
    leeftijd = "Leeftijd",
    meting = "Volgnummer binnen persoon",
    zin = "Zin in een sigaret",
    stress = "Mate van ervaren stress",
    roken = "Heeft u gerookt (ja/nee)",
    samen = "Bent u in gezelschap (alleen/samen)"
    );

for (i in names(variableLabels)) {
  attr(roken[, i], "label") <- variableLabels[i];
}

roken_vars <-
  data.frame(column = names(roken),
             variable = variableLabels);

knitr::kable(roken_vars,
             row.names = FALSE)

```
