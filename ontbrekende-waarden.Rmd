# Ontbrekende waarden {#ontbrekende-waarden}

## Intro

Bij het verzamelen van data, zullen er vaak ontbrekende data (missings) zijn. Hier kan op heel veel manieren mee worden omgegaan. Hier worden enkele basale aanpakken getoond. Allereerst de listwise en pairwise verwijderingsmethoden en daarna imputatiemethoden.

### Voorbeeld data

Voor dit voorbeeld zullen we de `contraProductiefGedrag` dataset gebruiken. Deze dataset staat beschreven in het hoofdstuk Datasets. We zullen ons voornamelijk focussen op het verbanden tussen `leeftijd`, `vertrouwen` en `cynisme`.

## jamovi

Jamovi gebruikt als standaard de pairwise verwijdering bij correlaties. Dit is te zien aan de $n$, die bij elk paar anders kan zijn als er missing waarden zijn.

```
jmv::corrMatrix(
    data = data,
    vars = vars(leeftijd, vertrouwen, cynisme),
    n = TRUE,
    plots = TRUE)
    
```

Om een analyse op complete observaties (listwise verwijderen) te doen, kan het symbool "Filters" in het tabblad "data" worden gebruikt. Definieer hierin de missings in de drie variabelen:
```
= leeftijd != NA and vertrouwen != NA and cynisme != NA
```
Hierdoor worden alle regels waarin een van deze drie variabelen een missing heeft verwijderd, dat wil zeggen niet meegenomen in de analyse. Dit is te zien aan de lichtgrijze arcering van deze regels.


### Imputeren


## R

Voor de berekenign van correlaties kan je het subcommando `use` gebruiken in de `cor` functie om aan te geven hoe er met misisngsmoet worden omgegaan. Pairwise verwijdering gaat via `use= "pairwise.complete.obs"` en listwise via  `use = "complete.obs"`.

```
cor(contraProductiefGedrag[,c("leeftijd,"cynisme","vertrouwen")], use = "complete.obs")

```

### Imputeren

In R bestaat het package "mice"




## SPSS

Standaard zal SPSS de complete case analyse toepassen (listwise verwijderen). Met het subcommando missing kan je aangeven hoe je wil dat SPSS met de ontbrekende waarden omgaat. Bijvooorbeeld door daar "pairwise" in te vullen. 

```
correlations leeftijd cynisme vertrouwen
/missing pairwise.
```

### Imputeren
