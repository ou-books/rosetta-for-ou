# Non-parametrische toetsen {#non-parametrische-toetsen}

## Intro

In sommige gevallen voldoe je niet aan de assumpties om de testen uit te voeren die je graag had willen doen. Zo geldt bijvoorbeeld voor de t-toetsen dat de variabele(n) normaal verdeeld moeten zijn. Indien dit niet het geval is, kun je alternatieven gebruiken. Eén van die alternatieven is het gebruik van nonparametrische toetsen. Voor een aantal veelvoorkomende statistische testen zijn nonparametrische alternatieven gemaakt.
Zo heb je voor de gepaarde t-toets als alternatief de Wilcoxon's signed rank test, voor de onafhankelijke t-toets de Wilcoxon's rank sum test (ook wel Mann-Whitney U test genoemd), en voor de one-way ANOVA de Kruskal-Wallis test.

### Voorbeeld dataset

Voor deze dataset zullen we gebruik maken van de dataset `statistiekangst`. Informatie over deze dataset is te vinden in \@ref(datasets) en informatie over hoe je data kunt laden staat beschreven in \@ref(data-laden). Voor de Wilcoxon's signed rank test zullen we kijken of er een verschil is tussen de statistiekangst voor en na het volgen van een cursus. Voor de Wilcoxon's rank sum test zullen we kijken of er een verschil is in de toename in statistiekangst tussen de twee cursussen, en voor de Kruskal-Wallis test zullen we kijken of er een verschil is in initiële statistiekangst tussen mensen van een verschillend opleidingsniveau.

## jamovi

### Wilcoxon's signed rank
In het tabblad "Analyses", klik op "T-tests". Kies vervolgens voor "Paired samples T-test". Linksonder is een kopje genaamd "Tests". Hier staat automatisch `student's ` aangevinkt. Dit is de normale t-test. Deselecteer deze en klik in plaats daarvan op `Wilcoxon rank`. Verplaats nu de score van het eerste meetmoment naar het vakje "Paired Variables" en vervolgens de score van het tweede meetmoment. Als het goed is staan deze scores nu naast elkaar in het vak en verschijnt rechts de uitkomst van de analyse.

De onderliggende syntax voor deze analyse is als volgt:

```
jmv::ttestPS(
    data = data,
    pairs = list(
        list(
            i1="Angst1",
            i2="Angst2")),
    students = FALSE,
    wilcoxon = TRUE)
```

### Mann-Whitney U
In het tabblad "Analyses", klik op "T-tests". Kies vervolgens voor "Independent samples T-test". Linksonder is een kopje genaamd "Tests". Hier staat automatisch `student's ` aangevinkt. Dit is de normale t-test. Deselecteer deze en klik in plaats daarvan op `Mann-Whitney U`. Verplaats nu de afhankelijke variabele naar het vak "Dependent Variables" en verplaats de dichotome variabele naar "Grouping Variables". Nu verschijnt aan de rechterhelft automatisch de uitkomst van de test.

De onderliggende syntax voor deze analyse is als volgt:

```
jmv::ttestIS(
    formula = dif_Angst ~ Cursus,
    data = data,
    vars = dif_Angst,
    students = FALSE,
    mann = TRUE)
```

### Kruskal-wallis
In het tabblad "Analyses", klik op "ANOVA". Vervolgens kies je uit het menu onderaan bij non-parametric voor "One-Way ANOVA Kruskal-Wallis". Verplaats de afhankelijke variabele naar het vak "Dependent variables" en verplaats de factor naar "Grouping Variable".

De onderliggende syntax voor deze analyse is als volgt:

``` 
jmv::anovaNP(
    formula = Angst1 ~ Education,
    data = data)
```

## R

In R kun je zowel de Wilcoxon's signed rank als de Wilcoxon's rank sum test uitvoeren met de functie `wilcox.test`. De Kruskal-wallis toets kunnen we uitvoeren met `kruskal.test`. Om meer te leren over extra argumenten die je kunt gebruiken om je test uit te breiden, bijvoorbeeld het instellen van een eenzijdige hypothese, kun je altijd kijken in de help functie door `?wilcox.test` in te typen.

### Wilcoxon's signed rank
Met onderstaande code kun je de Wilcoxon's signed rank test uitvoeren in R:

```
wilcox.test(mydata$Angst1, mydata$Angst2, paired= TRUE)
```

### Wilcoxon's rank sum
Met de onderstaande code kun je de Wilcoxon's rank sum test uitvoeren in R:

```
wilcox.test(dif_Angst ~ Cursus, data=mydata)
```

### Kruskal-Wallis
Met de Kruskal-Wallis test kunnen we meerdere groepen vergelijken. Hiervoor gebruiken we de volgende code:

```
kruskal.test(Angst1 ~ Education, data=mydata)
```

## SPSS

In SPSS zijn er twee manieren om tot een nonparametrische test te komen. Onder "Analyze", in het tabblad "Nonparametric tests" zijn drie categorieën te vinden: Related samples, Independent Samples en One Sample. Als je hierop klikt kom je in een vrij ingewikkeld keuzemenu waarbij je zelf testen kunt selecteren of SPSS laat bepalen welke test geschikt is voor uw data. Daaronder vindt je echter ook de optie `Legacy Dialogs`, en dan kun je direct kiezen om wat voor test het gaat.

### Wilcoxon's signed rank
Aangezien de Wilcoxon's signed rank test een test van gepaarde data is, klik bij Legacy Dialogs vervolgens op "2 Related Samples". Hier kun je vervolgens de paren aangeven en welke test je wilt doen, waarbij Wilcoxon automatisch is aangevinkt.
Je kunt ook de volgende syntax gebruiken:

```
NPAR TESTS
  /WILCOXON=Angst1 WITH Angst2 (PAIRED)
  /MISSING ANALYSIS.
``` 

### Mann-Whitney U
De Wilcoxon's rank sum of Mann-Whitney U test is een test voor onafhankelijke groepen. Kies bij Legacy Dialogs dan ook voor "2 Independent Samples". Hier kun je vervolgens aangeven wat de afhankelijke variabele is en welke groepen er zijn. Dit levert de volgende syntax op:

```
NPAR TESTS
  /M-W= dif_Angst BY Cursus
  /MISSING ANALYSIS.
```

### Kruskal-Wallis
De Kruskal-Wallis vergelijkt meerdere groepen op een afhankelijke variabele. Hiervoor kun je bij Legacy Dialogs klikken op "K Independent Samples". Selecteer de variabele die je wilt vergelijken en geef de range van de groepen aan. Dit levert de volgende syntax op:

```
NPAR TESTS
  /K-W= Angst1 BY Education(1 5)
  /MISSING ANALYSIS.
``` 
