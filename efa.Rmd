# Exploratorieve Factoranalyse {#efa}

## Intro

Exploratieve Factoranalyse (EFA) is een manier om te zoeken naar onderliggende factoren om een set variabelen te verklaren. Deze methode wordt vaak toegepast om schalen te construeren uit een grote set items. Deze set items wordt teruggebracht tot een kleiner aantal factoren.


### Voorbeeld data

Dit voorbeeld gebruikt de Rosetta Stats voorbeeld dataset "pp15" (Zie Hoofdstuk \@ref(datasets) voor informatie over de dataset en hoofdstuk \@ref(data-laden) voor een uitleg hoe datasets in te laden).


```{r efa-load-data}
data("pp15", package="rosetta");
```


```{r efa-define-backtick}
backtick <- "`"
```

Uit deze dataset gebruiken we de volgende variabelen: `r ufs::vecTxt(grep("highDose_AttBeliefs_", names(rosetta::pp15), value=TRUE)[1:9], useQuote=backtick);`.

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## jamovi

In het tabblad "Analyses", klik op de knop "Factor" en uit het menu dat verschijnt, kies "Exploratory Factor Analysis". Uit de variabelen in je dataset, selecteer de items die je wilt includeren en verplaats ze naar het vak genaamd "Variables" aan de rechterkant. Nu verschijnt automatisch de factoranalyse in het vak met de resultaten met de standaardinstellingen.
Onderaan de analyse kun je allerlei verschillende instellingen specificeren en aanpassen. Zo kun je bijvoorbeeld kiezen dat je alleen factoren wilt behouden met een eigenwaarde groter dan 1, dat je de correlatie tussen de factoren wilt zien, de scree plot, en nog meer extra's.

Uiteindelijk ziet de onderliggende code er dan als volgt uit:

```
jmv::efa(
    data = data,
    vars = vars(highDose_AttBeliefs_long, highDose_AttBeliefs_intensity, highDose_AttBeliefs_intoxicated, highDose_AttBeliefs_energy, highDose_AttBeliefs_euphoria, highDose_AttBeliefs_insight, highDose_AttBeliefs_connection, highDose_AttBeliefs_contact, highDose_AttBeliefs_sex),
    nFactorMethod = "eigen",
    screePlot = TRUE,
    factorCor = TRUE,
    factorScoresOV = list(
        synced=list()))
```

## R

In R kun je de `rosetta` package gebruiken om de EFA uit te voeren met het volgende commando:

```r
rosetta::factorAnalysis(
  data = dat,
  items = c(
    'highDose_AttBeliefs_long',
    'highDose_AttBeliefs_intensity',
    'highDose_AttBeliefs_intoxicated',
    'highDose_AttBeliefs_energy',
    'highDose_AttBeliefs_euphoria',
    'highDose_AttBeliefs_insight',
    'highDose_AttBeliefs_connection',
    'highDose_AttBeliefs_contact',
    'highDose_AttBeliefs_sex'
  ),
  nfactors = "eigen"
);
```
Let erop dat deze functie je verplicht om direct aan te geven hoeveel factoren je wilt overhouden aan het eind, met het `nfactors` argument. In het bovenstaande voorbeeld is dit gespecificeerd met "`eigen`", wat wil zeggen dat het Kaiser criterion gebruikt wordt. Dit criterion geeft je de mogelijkheid om de minimale eigenwaarde te bepalen (staat standaard ingesteld op `1`)

Om overige informatie aan te vragen, zoals een samenvatting, de correlaties tussen de factoren, een scree plot, of residuen, kun je extra opties specificeren in het commando. Ook kun je bijvoorbeeld item labels toevoegen aan je set met variabelen.

```r
rosetta::factorAnalysis(
  data = dat,
  items = c(
    'highDose_AttBeliefs_long',
    'highDose_AttBeliefs_intensity',
    'highDose_AttBeliefs_intoxicated',
    'highDose_AttBeliefs_energy',
    'highDose_AttBeliefs_euphoria',
    'highDose_AttBeliefs_insight',
    'highDose_AttBeliefs_connection',
    'highDose_AttBeliefs_contact',
    'highDose_AttBeliefs_sex'
  ),
  itemLabels = c(
    'Expectation that a high dose results in a longer trip',
    'Expectation that a high dose results in a more intense trip',
    'Expectation that a high dose makes you more intoxicated',
    'Expectation that a high dose provides more energy',
    'Expectation that a high dose produces more euphoria',
    'Expectation that a high dose yields more insight',
    'Expectation that a high dose strengthens your connection with others',
    'Expectation that a high dose facilitates making contact with others',
    'Expectation that a high dose improves sex'
  ),
  nfactors = "eigen",
  summary = TRUE,
  correlations = TRUE,
  scree = TRUE,
  residuals = TRUE
);
```

### SPSS

In SPSS kun je het `FACTOR` commando gebruiken. Belangrijke argumenten om te gebruiken zijn `/VARIABLES` om de items te specificeren, `/CRITERIA` om te bepalen hoeveel factoren er moeten overblijven (of hoe dat beslist moet worden: gebruik `MINEIGEN(1)` om alleen factoren met een eigenwaarde hoger dan 1, of bijvoorbeeld `FACTORS(2)` om 2 factoren te extraheren), `/EXTRACTION` om de manier waarop de factoren worden berekend te kiezen (zoals `OLS` voor ordinary least squares regressie, of `ML` voor de maximum likelihood methode), en `/ROTATION` om de rotatie vast te stellen (bijv. `NOROTATE` als je geen rotatie wilt, of `VARIMAX` als je orthogonale rotatie wilt).


```
FACTOR
  /VARIABLES
    highDose_AttBeliefs_long
    highDose_AttBeliefs_intensity
    highDose_AttBeliefs_intoxicated
    highDose_AttBeliefs_energy
    highDose_AttBeliefs_euphoria
    highDose_AttBeliefs_insight
    highDose_AttBeliefs_connection
    highDose_AttBeliefs_contact
    highDose_AttBeliefs_sex
  /CRITERIA =
    MINEIGEN(1)
  /EXTRACTION =
    ULS
  /ROTATION =
    OBLIMIN
.
```

Voor specifieke uitkomsten kun je `/PRINT` en `/PLOT` gebruiken om bijvoorbeeld een scree plot te zien:

```
FACTOR
  /VARIABLES
    highDose_AttBeliefs_long
    highDose_AttBeliefs_intensity
    highDose_AttBeliefs_intoxicated
    highDose_AttBeliefs_energy
    highDose_AttBeliefs_euphoria
    highDose_AttBeliefs_insight
    highDose_AttBeliefs_connection
    highDose_AttBeliefs_contact
    highDose_AttBeliefs_sex
  /CRITERIA =
    FACTORS(1)
  /PRINT =
    INITIAL
    EXTRACTION
    UNIVARIATE
    CORRELATION
    REPR
  /PLOT =
    EIGEN
  /EXTRACTION =
    ULS
  /ROTATION =
    OBLIMIN
.
```

Meer informatie is te vinden in de SPSS manual:

[The SPSS FACTOR manual section](https://www.ibm.com/support/knowledgecenter/en/SSLVMB_24.0.0/spss/base/syn_factor_overview.html)
